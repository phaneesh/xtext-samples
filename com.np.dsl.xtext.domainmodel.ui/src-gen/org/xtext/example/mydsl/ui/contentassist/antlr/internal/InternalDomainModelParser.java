package org.xtext.example.mydsl.ui.contentassist.antlr.internal; 

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.DFA;
import org.xtext.example.mydsl.services.DomainModelGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalDomainModelParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'datatype'", "'entity'", "'{'", "'}'", "'extends'", "':'", "'many'"
    };
    public static final int RULE_ID=4;
    public static final int RULE_STRING=6;
    public static final int T__16=16;
    public static final int T__15=15;
    public static final int T__17=17;
    public static final int T__12=12;
    public static final int T__11=11;
    public static final int T__14=14;
    public static final int T__13=13;
    public static final int RULE_ANY_OTHER=10;
    public static final int RULE_INT=5;
    public static final int RULE_WS=9;
    public static final int RULE_SL_COMMENT=8;
    public static final int EOF=-1;
    public static final int RULE_ML_COMMENT=7;

    // delegates
    // delegators


        public InternalDomainModelParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalDomainModelParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalDomainModelParser.tokenNames; }
    public String getGrammarFileName() { return "../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g"; }


     
     	private DomainModelGrammarAccess grammarAccess;
     	
        public void setGrammarAccess(DomainModelGrammarAccess grammarAccess) {
        	this.grammarAccess = grammarAccess;
        }
        
        @Override
        protected Grammar getGrammar() {
        	return grammarAccess.getGrammar();
        }
        
        @Override
        protected String getValueForTokenName(String tokenName) {
        	return tokenName;
        }




    // $ANTLR start "entryRuleDomainmodel"
    // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:60:1: entryRuleDomainmodel : ruleDomainmodel EOF ;
    public final void entryRuleDomainmodel() throws RecognitionException {
        try {
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:61:1: ( ruleDomainmodel EOF )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:62:1: ruleDomainmodel EOF
            {
             before(grammarAccess.getDomainmodelRule()); 
            pushFollow(FOLLOW_ruleDomainmodel_in_entryRuleDomainmodel61);
            ruleDomainmodel();

            state._fsp--;

             after(grammarAccess.getDomainmodelRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleDomainmodel68); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDomainmodel"


    // $ANTLR start "ruleDomainmodel"
    // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:69:1: ruleDomainmodel : ( ( rule__Domainmodel__ElementsAssignment )* ) ;
    public final void ruleDomainmodel() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:73:2: ( ( ( rule__Domainmodel__ElementsAssignment )* ) )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:74:1: ( ( rule__Domainmodel__ElementsAssignment )* )
            {
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:74:1: ( ( rule__Domainmodel__ElementsAssignment )* )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:75:1: ( rule__Domainmodel__ElementsAssignment )*
            {
             before(grammarAccess.getDomainmodelAccess().getElementsAssignment()); 
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:76:1: ( rule__Domainmodel__ElementsAssignment )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( ((LA1_0>=11 && LA1_0<=12)) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:76:2: rule__Domainmodel__ElementsAssignment
            	    {
            	    pushFollow(FOLLOW_rule__Domainmodel__ElementsAssignment_in_ruleDomainmodel94);
            	    rule__Domainmodel__ElementsAssignment();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

             after(grammarAccess.getDomainmodelAccess().getElementsAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDomainmodel"


    // $ANTLR start "entryRuleType"
    // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:88:1: entryRuleType : ruleType EOF ;
    public final void entryRuleType() throws RecognitionException {
        try {
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:89:1: ( ruleType EOF )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:90:1: ruleType EOF
            {
             before(grammarAccess.getTypeRule()); 
            pushFollow(FOLLOW_ruleType_in_entryRuleType122);
            ruleType();

            state._fsp--;

             after(grammarAccess.getTypeRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleType129); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleType"


    // $ANTLR start "ruleType"
    // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:97:1: ruleType : ( ( rule__Type__Alternatives ) ) ;
    public final void ruleType() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:101:2: ( ( ( rule__Type__Alternatives ) ) )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:102:1: ( ( rule__Type__Alternatives ) )
            {
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:102:1: ( ( rule__Type__Alternatives ) )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:103:1: ( rule__Type__Alternatives )
            {
             before(grammarAccess.getTypeAccess().getAlternatives()); 
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:104:1: ( rule__Type__Alternatives )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:104:2: rule__Type__Alternatives
            {
            pushFollow(FOLLOW_rule__Type__Alternatives_in_ruleType155);
            rule__Type__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getTypeAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleType"


    // $ANTLR start "entryRuleDataType"
    // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:116:1: entryRuleDataType : ruleDataType EOF ;
    public final void entryRuleDataType() throws RecognitionException {
        try {
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:117:1: ( ruleDataType EOF )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:118:1: ruleDataType EOF
            {
             before(grammarAccess.getDataTypeRule()); 
            pushFollow(FOLLOW_ruleDataType_in_entryRuleDataType182);
            ruleDataType();

            state._fsp--;

             after(grammarAccess.getDataTypeRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleDataType189); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDataType"


    // $ANTLR start "ruleDataType"
    // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:125:1: ruleDataType : ( ( rule__DataType__Group__0 ) ) ;
    public final void ruleDataType() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:129:2: ( ( ( rule__DataType__Group__0 ) ) )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:130:1: ( ( rule__DataType__Group__0 ) )
            {
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:130:1: ( ( rule__DataType__Group__0 ) )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:131:1: ( rule__DataType__Group__0 )
            {
             before(grammarAccess.getDataTypeAccess().getGroup()); 
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:132:1: ( rule__DataType__Group__0 )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:132:2: rule__DataType__Group__0
            {
            pushFollow(FOLLOW_rule__DataType__Group__0_in_ruleDataType215);
            rule__DataType__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getDataTypeAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDataType"


    // $ANTLR start "entryRuleEntity"
    // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:144:1: entryRuleEntity : ruleEntity EOF ;
    public final void entryRuleEntity() throws RecognitionException {
        try {
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:145:1: ( ruleEntity EOF )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:146:1: ruleEntity EOF
            {
             before(grammarAccess.getEntityRule()); 
            pushFollow(FOLLOW_ruleEntity_in_entryRuleEntity242);
            ruleEntity();

            state._fsp--;

             after(grammarAccess.getEntityRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleEntity249); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEntity"


    // $ANTLR start "ruleEntity"
    // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:153:1: ruleEntity : ( ( rule__Entity__Group__0 ) ) ;
    public final void ruleEntity() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:157:2: ( ( ( rule__Entity__Group__0 ) ) )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:158:1: ( ( rule__Entity__Group__0 ) )
            {
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:158:1: ( ( rule__Entity__Group__0 ) )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:159:1: ( rule__Entity__Group__0 )
            {
             before(grammarAccess.getEntityAccess().getGroup()); 
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:160:1: ( rule__Entity__Group__0 )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:160:2: rule__Entity__Group__0
            {
            pushFollow(FOLLOW_rule__Entity__Group__0_in_ruleEntity275);
            rule__Entity__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getEntityAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEntity"


    // $ANTLR start "entryRuleFeature"
    // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:172:1: entryRuleFeature : ruleFeature EOF ;
    public final void entryRuleFeature() throws RecognitionException {
        try {
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:173:1: ( ruleFeature EOF )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:174:1: ruleFeature EOF
            {
             before(grammarAccess.getFeatureRule()); 
            pushFollow(FOLLOW_ruleFeature_in_entryRuleFeature302);
            ruleFeature();

            state._fsp--;

             after(grammarAccess.getFeatureRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleFeature309); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleFeature"


    // $ANTLR start "ruleFeature"
    // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:181:1: ruleFeature : ( ( rule__Feature__Group__0 ) ) ;
    public final void ruleFeature() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:185:2: ( ( ( rule__Feature__Group__0 ) ) )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:186:1: ( ( rule__Feature__Group__0 ) )
            {
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:186:1: ( ( rule__Feature__Group__0 ) )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:187:1: ( rule__Feature__Group__0 )
            {
             before(grammarAccess.getFeatureAccess().getGroup()); 
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:188:1: ( rule__Feature__Group__0 )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:188:2: rule__Feature__Group__0
            {
            pushFollow(FOLLOW_rule__Feature__Group__0_in_ruleFeature335);
            rule__Feature__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getFeatureAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFeature"


    // $ANTLR start "rule__Type__Alternatives"
    // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:200:1: rule__Type__Alternatives : ( ( ruleDataType ) | ( ruleEntity ) );
    public final void rule__Type__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:204:1: ( ( ruleDataType ) | ( ruleEntity ) )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==11) ) {
                alt2=1;
            }
            else if ( (LA2_0==12) ) {
                alt2=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:205:1: ( ruleDataType )
                    {
                    // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:205:1: ( ruleDataType )
                    // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:206:1: ruleDataType
                    {
                     before(grammarAccess.getTypeAccess().getDataTypeParserRuleCall_0()); 
                    pushFollow(FOLLOW_ruleDataType_in_rule__Type__Alternatives371);
                    ruleDataType();

                    state._fsp--;

                     after(grammarAccess.getTypeAccess().getDataTypeParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:211:6: ( ruleEntity )
                    {
                    // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:211:6: ( ruleEntity )
                    // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:212:1: ruleEntity
                    {
                     before(grammarAccess.getTypeAccess().getEntityParserRuleCall_1()); 
                    pushFollow(FOLLOW_ruleEntity_in_rule__Type__Alternatives388);
                    ruleEntity();

                    state._fsp--;

                     after(grammarAccess.getTypeAccess().getEntityParserRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Type__Alternatives"


    // $ANTLR start "rule__DataType__Group__0"
    // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:224:1: rule__DataType__Group__0 : rule__DataType__Group__0__Impl rule__DataType__Group__1 ;
    public final void rule__DataType__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:228:1: ( rule__DataType__Group__0__Impl rule__DataType__Group__1 )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:229:2: rule__DataType__Group__0__Impl rule__DataType__Group__1
            {
            pushFollow(FOLLOW_rule__DataType__Group__0__Impl_in_rule__DataType__Group__0418);
            rule__DataType__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__DataType__Group__1_in_rule__DataType__Group__0421);
            rule__DataType__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataType__Group__0"


    // $ANTLR start "rule__DataType__Group__0__Impl"
    // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:236:1: rule__DataType__Group__0__Impl : ( 'datatype' ) ;
    public final void rule__DataType__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:240:1: ( ( 'datatype' ) )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:241:1: ( 'datatype' )
            {
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:241:1: ( 'datatype' )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:242:1: 'datatype'
            {
             before(grammarAccess.getDataTypeAccess().getDatatypeKeyword_0()); 
            match(input,11,FOLLOW_11_in_rule__DataType__Group__0__Impl449); 
             after(grammarAccess.getDataTypeAccess().getDatatypeKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataType__Group__0__Impl"


    // $ANTLR start "rule__DataType__Group__1"
    // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:255:1: rule__DataType__Group__1 : rule__DataType__Group__1__Impl ;
    public final void rule__DataType__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:259:1: ( rule__DataType__Group__1__Impl )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:260:2: rule__DataType__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__DataType__Group__1__Impl_in_rule__DataType__Group__1480);
            rule__DataType__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataType__Group__1"


    // $ANTLR start "rule__DataType__Group__1__Impl"
    // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:266:1: rule__DataType__Group__1__Impl : ( ( rule__DataType__NameAssignment_1 ) ) ;
    public final void rule__DataType__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:270:1: ( ( ( rule__DataType__NameAssignment_1 ) ) )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:271:1: ( ( rule__DataType__NameAssignment_1 ) )
            {
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:271:1: ( ( rule__DataType__NameAssignment_1 ) )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:272:1: ( rule__DataType__NameAssignment_1 )
            {
             before(grammarAccess.getDataTypeAccess().getNameAssignment_1()); 
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:273:1: ( rule__DataType__NameAssignment_1 )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:273:2: rule__DataType__NameAssignment_1
            {
            pushFollow(FOLLOW_rule__DataType__NameAssignment_1_in_rule__DataType__Group__1__Impl507);
            rule__DataType__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getDataTypeAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataType__Group__1__Impl"


    // $ANTLR start "rule__Entity__Group__0"
    // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:287:1: rule__Entity__Group__0 : rule__Entity__Group__0__Impl rule__Entity__Group__1 ;
    public final void rule__Entity__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:291:1: ( rule__Entity__Group__0__Impl rule__Entity__Group__1 )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:292:2: rule__Entity__Group__0__Impl rule__Entity__Group__1
            {
            pushFollow(FOLLOW_rule__Entity__Group__0__Impl_in_rule__Entity__Group__0541);
            rule__Entity__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Entity__Group__1_in_rule__Entity__Group__0544);
            rule__Entity__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__Group__0"


    // $ANTLR start "rule__Entity__Group__0__Impl"
    // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:299:1: rule__Entity__Group__0__Impl : ( 'entity' ) ;
    public final void rule__Entity__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:303:1: ( ( 'entity' ) )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:304:1: ( 'entity' )
            {
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:304:1: ( 'entity' )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:305:1: 'entity'
            {
             before(grammarAccess.getEntityAccess().getEntityKeyword_0()); 
            match(input,12,FOLLOW_12_in_rule__Entity__Group__0__Impl572); 
             after(grammarAccess.getEntityAccess().getEntityKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__Group__0__Impl"


    // $ANTLR start "rule__Entity__Group__1"
    // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:318:1: rule__Entity__Group__1 : rule__Entity__Group__1__Impl rule__Entity__Group__2 ;
    public final void rule__Entity__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:322:1: ( rule__Entity__Group__1__Impl rule__Entity__Group__2 )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:323:2: rule__Entity__Group__1__Impl rule__Entity__Group__2
            {
            pushFollow(FOLLOW_rule__Entity__Group__1__Impl_in_rule__Entity__Group__1603);
            rule__Entity__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Entity__Group__2_in_rule__Entity__Group__1606);
            rule__Entity__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__Group__1"


    // $ANTLR start "rule__Entity__Group__1__Impl"
    // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:330:1: rule__Entity__Group__1__Impl : ( ( rule__Entity__NameAssignment_1 ) ) ;
    public final void rule__Entity__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:334:1: ( ( ( rule__Entity__NameAssignment_1 ) ) )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:335:1: ( ( rule__Entity__NameAssignment_1 ) )
            {
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:335:1: ( ( rule__Entity__NameAssignment_1 ) )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:336:1: ( rule__Entity__NameAssignment_1 )
            {
             before(grammarAccess.getEntityAccess().getNameAssignment_1()); 
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:337:1: ( rule__Entity__NameAssignment_1 )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:337:2: rule__Entity__NameAssignment_1
            {
            pushFollow(FOLLOW_rule__Entity__NameAssignment_1_in_rule__Entity__Group__1__Impl633);
            rule__Entity__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getEntityAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__Group__1__Impl"


    // $ANTLR start "rule__Entity__Group__2"
    // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:347:1: rule__Entity__Group__2 : rule__Entity__Group__2__Impl rule__Entity__Group__3 ;
    public final void rule__Entity__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:351:1: ( rule__Entity__Group__2__Impl rule__Entity__Group__3 )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:352:2: rule__Entity__Group__2__Impl rule__Entity__Group__3
            {
            pushFollow(FOLLOW_rule__Entity__Group__2__Impl_in_rule__Entity__Group__2663);
            rule__Entity__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Entity__Group__3_in_rule__Entity__Group__2666);
            rule__Entity__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__Group__2"


    // $ANTLR start "rule__Entity__Group__2__Impl"
    // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:359:1: rule__Entity__Group__2__Impl : ( ( rule__Entity__Group_2__0 )? ) ;
    public final void rule__Entity__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:363:1: ( ( ( rule__Entity__Group_2__0 )? ) )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:364:1: ( ( rule__Entity__Group_2__0 )? )
            {
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:364:1: ( ( rule__Entity__Group_2__0 )? )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:365:1: ( rule__Entity__Group_2__0 )?
            {
             before(grammarAccess.getEntityAccess().getGroup_2()); 
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:366:1: ( rule__Entity__Group_2__0 )?
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==15) ) {
                alt3=1;
            }
            switch (alt3) {
                case 1 :
                    // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:366:2: rule__Entity__Group_2__0
                    {
                    pushFollow(FOLLOW_rule__Entity__Group_2__0_in_rule__Entity__Group__2__Impl693);
                    rule__Entity__Group_2__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getEntityAccess().getGroup_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__Group__2__Impl"


    // $ANTLR start "rule__Entity__Group__3"
    // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:376:1: rule__Entity__Group__3 : rule__Entity__Group__3__Impl rule__Entity__Group__4 ;
    public final void rule__Entity__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:380:1: ( rule__Entity__Group__3__Impl rule__Entity__Group__4 )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:381:2: rule__Entity__Group__3__Impl rule__Entity__Group__4
            {
            pushFollow(FOLLOW_rule__Entity__Group__3__Impl_in_rule__Entity__Group__3724);
            rule__Entity__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Entity__Group__4_in_rule__Entity__Group__3727);
            rule__Entity__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__Group__3"


    // $ANTLR start "rule__Entity__Group__3__Impl"
    // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:388:1: rule__Entity__Group__3__Impl : ( '{' ) ;
    public final void rule__Entity__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:392:1: ( ( '{' ) )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:393:1: ( '{' )
            {
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:393:1: ( '{' )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:394:1: '{'
            {
             before(grammarAccess.getEntityAccess().getLeftCurlyBracketKeyword_3()); 
            match(input,13,FOLLOW_13_in_rule__Entity__Group__3__Impl755); 
             after(grammarAccess.getEntityAccess().getLeftCurlyBracketKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__Group__3__Impl"


    // $ANTLR start "rule__Entity__Group__4"
    // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:407:1: rule__Entity__Group__4 : rule__Entity__Group__4__Impl rule__Entity__Group__5 ;
    public final void rule__Entity__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:411:1: ( rule__Entity__Group__4__Impl rule__Entity__Group__5 )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:412:2: rule__Entity__Group__4__Impl rule__Entity__Group__5
            {
            pushFollow(FOLLOW_rule__Entity__Group__4__Impl_in_rule__Entity__Group__4786);
            rule__Entity__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Entity__Group__5_in_rule__Entity__Group__4789);
            rule__Entity__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__Group__4"


    // $ANTLR start "rule__Entity__Group__4__Impl"
    // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:419:1: rule__Entity__Group__4__Impl : ( ( rule__Entity__FeaturesAssignment_4 )* ) ;
    public final void rule__Entity__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:423:1: ( ( ( rule__Entity__FeaturesAssignment_4 )* ) )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:424:1: ( ( rule__Entity__FeaturesAssignment_4 )* )
            {
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:424:1: ( ( rule__Entity__FeaturesAssignment_4 )* )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:425:1: ( rule__Entity__FeaturesAssignment_4 )*
            {
             before(grammarAccess.getEntityAccess().getFeaturesAssignment_4()); 
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:426:1: ( rule__Entity__FeaturesAssignment_4 )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==RULE_ID||LA4_0==17) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:426:2: rule__Entity__FeaturesAssignment_4
            	    {
            	    pushFollow(FOLLOW_rule__Entity__FeaturesAssignment_4_in_rule__Entity__Group__4__Impl816);
            	    rule__Entity__FeaturesAssignment_4();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);

             after(grammarAccess.getEntityAccess().getFeaturesAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__Group__4__Impl"


    // $ANTLR start "rule__Entity__Group__5"
    // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:436:1: rule__Entity__Group__5 : rule__Entity__Group__5__Impl ;
    public final void rule__Entity__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:440:1: ( rule__Entity__Group__5__Impl )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:441:2: rule__Entity__Group__5__Impl
            {
            pushFollow(FOLLOW_rule__Entity__Group__5__Impl_in_rule__Entity__Group__5847);
            rule__Entity__Group__5__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__Group__5"


    // $ANTLR start "rule__Entity__Group__5__Impl"
    // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:447:1: rule__Entity__Group__5__Impl : ( '}' ) ;
    public final void rule__Entity__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:451:1: ( ( '}' ) )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:452:1: ( '}' )
            {
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:452:1: ( '}' )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:453:1: '}'
            {
             before(grammarAccess.getEntityAccess().getRightCurlyBracketKeyword_5()); 
            match(input,14,FOLLOW_14_in_rule__Entity__Group__5__Impl875); 
             after(grammarAccess.getEntityAccess().getRightCurlyBracketKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__Group__5__Impl"


    // $ANTLR start "rule__Entity__Group_2__0"
    // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:478:1: rule__Entity__Group_2__0 : rule__Entity__Group_2__0__Impl rule__Entity__Group_2__1 ;
    public final void rule__Entity__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:482:1: ( rule__Entity__Group_2__0__Impl rule__Entity__Group_2__1 )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:483:2: rule__Entity__Group_2__0__Impl rule__Entity__Group_2__1
            {
            pushFollow(FOLLOW_rule__Entity__Group_2__0__Impl_in_rule__Entity__Group_2__0918);
            rule__Entity__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Entity__Group_2__1_in_rule__Entity__Group_2__0921);
            rule__Entity__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__Group_2__0"


    // $ANTLR start "rule__Entity__Group_2__0__Impl"
    // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:490:1: rule__Entity__Group_2__0__Impl : ( 'extends' ) ;
    public final void rule__Entity__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:494:1: ( ( 'extends' ) )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:495:1: ( 'extends' )
            {
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:495:1: ( 'extends' )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:496:1: 'extends'
            {
             before(grammarAccess.getEntityAccess().getExtendsKeyword_2_0()); 
            match(input,15,FOLLOW_15_in_rule__Entity__Group_2__0__Impl949); 
             after(grammarAccess.getEntityAccess().getExtendsKeyword_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__Group_2__0__Impl"


    // $ANTLR start "rule__Entity__Group_2__1"
    // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:509:1: rule__Entity__Group_2__1 : rule__Entity__Group_2__1__Impl ;
    public final void rule__Entity__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:513:1: ( rule__Entity__Group_2__1__Impl )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:514:2: rule__Entity__Group_2__1__Impl
            {
            pushFollow(FOLLOW_rule__Entity__Group_2__1__Impl_in_rule__Entity__Group_2__1980);
            rule__Entity__Group_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__Group_2__1"


    // $ANTLR start "rule__Entity__Group_2__1__Impl"
    // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:520:1: rule__Entity__Group_2__1__Impl : ( ( rule__Entity__SuperTypeAssignment_2_1 ) ) ;
    public final void rule__Entity__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:524:1: ( ( ( rule__Entity__SuperTypeAssignment_2_1 ) ) )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:525:1: ( ( rule__Entity__SuperTypeAssignment_2_1 ) )
            {
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:525:1: ( ( rule__Entity__SuperTypeAssignment_2_1 ) )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:526:1: ( rule__Entity__SuperTypeAssignment_2_1 )
            {
             before(grammarAccess.getEntityAccess().getSuperTypeAssignment_2_1()); 
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:527:1: ( rule__Entity__SuperTypeAssignment_2_1 )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:527:2: rule__Entity__SuperTypeAssignment_2_1
            {
            pushFollow(FOLLOW_rule__Entity__SuperTypeAssignment_2_1_in_rule__Entity__Group_2__1__Impl1007);
            rule__Entity__SuperTypeAssignment_2_1();

            state._fsp--;


            }

             after(grammarAccess.getEntityAccess().getSuperTypeAssignment_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__Group_2__1__Impl"


    // $ANTLR start "rule__Feature__Group__0"
    // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:541:1: rule__Feature__Group__0 : rule__Feature__Group__0__Impl rule__Feature__Group__1 ;
    public final void rule__Feature__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:545:1: ( rule__Feature__Group__0__Impl rule__Feature__Group__1 )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:546:2: rule__Feature__Group__0__Impl rule__Feature__Group__1
            {
            pushFollow(FOLLOW_rule__Feature__Group__0__Impl_in_rule__Feature__Group__01041);
            rule__Feature__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Feature__Group__1_in_rule__Feature__Group__01044);
            rule__Feature__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__Group__0"


    // $ANTLR start "rule__Feature__Group__0__Impl"
    // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:553:1: rule__Feature__Group__0__Impl : ( ( rule__Feature__ManyAssignment_0 )? ) ;
    public final void rule__Feature__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:557:1: ( ( ( rule__Feature__ManyAssignment_0 )? ) )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:558:1: ( ( rule__Feature__ManyAssignment_0 )? )
            {
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:558:1: ( ( rule__Feature__ManyAssignment_0 )? )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:559:1: ( rule__Feature__ManyAssignment_0 )?
            {
             before(grammarAccess.getFeatureAccess().getManyAssignment_0()); 
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:560:1: ( rule__Feature__ManyAssignment_0 )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==17) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:560:2: rule__Feature__ManyAssignment_0
                    {
                    pushFollow(FOLLOW_rule__Feature__ManyAssignment_0_in_rule__Feature__Group__0__Impl1071);
                    rule__Feature__ManyAssignment_0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getFeatureAccess().getManyAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__Group__0__Impl"


    // $ANTLR start "rule__Feature__Group__1"
    // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:570:1: rule__Feature__Group__1 : rule__Feature__Group__1__Impl rule__Feature__Group__2 ;
    public final void rule__Feature__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:574:1: ( rule__Feature__Group__1__Impl rule__Feature__Group__2 )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:575:2: rule__Feature__Group__1__Impl rule__Feature__Group__2
            {
            pushFollow(FOLLOW_rule__Feature__Group__1__Impl_in_rule__Feature__Group__11102);
            rule__Feature__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Feature__Group__2_in_rule__Feature__Group__11105);
            rule__Feature__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__Group__1"


    // $ANTLR start "rule__Feature__Group__1__Impl"
    // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:582:1: rule__Feature__Group__1__Impl : ( ( rule__Feature__NameAssignment_1 ) ) ;
    public final void rule__Feature__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:586:1: ( ( ( rule__Feature__NameAssignment_1 ) ) )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:587:1: ( ( rule__Feature__NameAssignment_1 ) )
            {
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:587:1: ( ( rule__Feature__NameAssignment_1 ) )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:588:1: ( rule__Feature__NameAssignment_1 )
            {
             before(grammarAccess.getFeatureAccess().getNameAssignment_1()); 
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:589:1: ( rule__Feature__NameAssignment_1 )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:589:2: rule__Feature__NameAssignment_1
            {
            pushFollow(FOLLOW_rule__Feature__NameAssignment_1_in_rule__Feature__Group__1__Impl1132);
            rule__Feature__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getFeatureAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__Group__1__Impl"


    // $ANTLR start "rule__Feature__Group__2"
    // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:599:1: rule__Feature__Group__2 : rule__Feature__Group__2__Impl rule__Feature__Group__3 ;
    public final void rule__Feature__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:603:1: ( rule__Feature__Group__2__Impl rule__Feature__Group__3 )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:604:2: rule__Feature__Group__2__Impl rule__Feature__Group__3
            {
            pushFollow(FOLLOW_rule__Feature__Group__2__Impl_in_rule__Feature__Group__21162);
            rule__Feature__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Feature__Group__3_in_rule__Feature__Group__21165);
            rule__Feature__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__Group__2"


    // $ANTLR start "rule__Feature__Group__2__Impl"
    // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:611:1: rule__Feature__Group__2__Impl : ( ':' ) ;
    public final void rule__Feature__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:615:1: ( ( ':' ) )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:616:1: ( ':' )
            {
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:616:1: ( ':' )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:617:1: ':'
            {
             before(grammarAccess.getFeatureAccess().getColonKeyword_2()); 
            match(input,16,FOLLOW_16_in_rule__Feature__Group__2__Impl1193); 
             after(grammarAccess.getFeatureAccess().getColonKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__Group__2__Impl"


    // $ANTLR start "rule__Feature__Group__3"
    // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:630:1: rule__Feature__Group__3 : rule__Feature__Group__3__Impl ;
    public final void rule__Feature__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:634:1: ( rule__Feature__Group__3__Impl )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:635:2: rule__Feature__Group__3__Impl
            {
            pushFollow(FOLLOW_rule__Feature__Group__3__Impl_in_rule__Feature__Group__31224);
            rule__Feature__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__Group__3"


    // $ANTLR start "rule__Feature__Group__3__Impl"
    // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:641:1: rule__Feature__Group__3__Impl : ( ( rule__Feature__TypeAssignment_3 ) ) ;
    public final void rule__Feature__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:645:1: ( ( ( rule__Feature__TypeAssignment_3 ) ) )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:646:1: ( ( rule__Feature__TypeAssignment_3 ) )
            {
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:646:1: ( ( rule__Feature__TypeAssignment_3 ) )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:647:1: ( rule__Feature__TypeAssignment_3 )
            {
             before(grammarAccess.getFeatureAccess().getTypeAssignment_3()); 
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:648:1: ( rule__Feature__TypeAssignment_3 )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:648:2: rule__Feature__TypeAssignment_3
            {
            pushFollow(FOLLOW_rule__Feature__TypeAssignment_3_in_rule__Feature__Group__3__Impl1251);
            rule__Feature__TypeAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getFeatureAccess().getTypeAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__Group__3__Impl"


    // $ANTLR start "rule__Domainmodel__ElementsAssignment"
    // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:667:1: rule__Domainmodel__ElementsAssignment : ( ruleType ) ;
    public final void rule__Domainmodel__ElementsAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:671:1: ( ( ruleType ) )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:672:1: ( ruleType )
            {
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:672:1: ( ruleType )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:673:1: ruleType
            {
             before(grammarAccess.getDomainmodelAccess().getElementsTypeParserRuleCall_0()); 
            pushFollow(FOLLOW_ruleType_in_rule__Domainmodel__ElementsAssignment1294);
            ruleType();

            state._fsp--;

             after(grammarAccess.getDomainmodelAccess().getElementsTypeParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Domainmodel__ElementsAssignment"


    // $ANTLR start "rule__DataType__NameAssignment_1"
    // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:682:1: rule__DataType__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__DataType__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:686:1: ( ( RULE_ID ) )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:687:1: ( RULE_ID )
            {
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:687:1: ( RULE_ID )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:688:1: RULE_ID
            {
             before(grammarAccess.getDataTypeAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__DataType__NameAssignment_11325); 
             after(grammarAccess.getDataTypeAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataType__NameAssignment_1"


    // $ANTLR start "rule__Entity__NameAssignment_1"
    // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:697:1: rule__Entity__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__Entity__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:701:1: ( ( RULE_ID ) )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:702:1: ( RULE_ID )
            {
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:702:1: ( RULE_ID )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:703:1: RULE_ID
            {
             before(grammarAccess.getEntityAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Entity__NameAssignment_11356); 
             after(grammarAccess.getEntityAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__NameAssignment_1"


    // $ANTLR start "rule__Entity__SuperTypeAssignment_2_1"
    // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:712:1: rule__Entity__SuperTypeAssignment_2_1 : ( ( RULE_ID ) ) ;
    public final void rule__Entity__SuperTypeAssignment_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:716:1: ( ( ( RULE_ID ) ) )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:717:1: ( ( RULE_ID ) )
            {
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:717:1: ( ( RULE_ID ) )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:718:1: ( RULE_ID )
            {
             before(grammarAccess.getEntityAccess().getSuperTypeEntityCrossReference_2_1_0()); 
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:719:1: ( RULE_ID )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:720:1: RULE_ID
            {
             before(grammarAccess.getEntityAccess().getSuperTypeEntityIDTerminalRuleCall_2_1_0_1()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Entity__SuperTypeAssignment_2_11391); 
             after(grammarAccess.getEntityAccess().getSuperTypeEntityIDTerminalRuleCall_2_1_0_1()); 

            }

             after(grammarAccess.getEntityAccess().getSuperTypeEntityCrossReference_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__SuperTypeAssignment_2_1"


    // $ANTLR start "rule__Entity__FeaturesAssignment_4"
    // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:731:1: rule__Entity__FeaturesAssignment_4 : ( ruleFeature ) ;
    public final void rule__Entity__FeaturesAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:735:1: ( ( ruleFeature ) )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:736:1: ( ruleFeature )
            {
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:736:1: ( ruleFeature )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:737:1: ruleFeature
            {
             before(grammarAccess.getEntityAccess().getFeaturesFeatureParserRuleCall_4_0()); 
            pushFollow(FOLLOW_ruleFeature_in_rule__Entity__FeaturesAssignment_41426);
            ruleFeature();

            state._fsp--;

             after(grammarAccess.getEntityAccess().getFeaturesFeatureParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__FeaturesAssignment_4"


    // $ANTLR start "rule__Feature__ManyAssignment_0"
    // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:746:1: rule__Feature__ManyAssignment_0 : ( ( 'many' ) ) ;
    public final void rule__Feature__ManyAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:750:1: ( ( ( 'many' ) ) )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:751:1: ( ( 'many' ) )
            {
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:751:1: ( ( 'many' ) )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:752:1: ( 'many' )
            {
             before(grammarAccess.getFeatureAccess().getManyManyKeyword_0_0()); 
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:753:1: ( 'many' )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:754:1: 'many'
            {
             before(grammarAccess.getFeatureAccess().getManyManyKeyword_0_0()); 
            match(input,17,FOLLOW_17_in_rule__Feature__ManyAssignment_01462); 
             after(grammarAccess.getFeatureAccess().getManyManyKeyword_0_0()); 

            }

             after(grammarAccess.getFeatureAccess().getManyManyKeyword_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__ManyAssignment_0"


    // $ANTLR start "rule__Feature__NameAssignment_1"
    // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:769:1: rule__Feature__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__Feature__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:773:1: ( ( RULE_ID ) )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:774:1: ( RULE_ID )
            {
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:774:1: ( RULE_ID )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:775:1: RULE_ID
            {
             before(grammarAccess.getFeatureAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Feature__NameAssignment_11501); 
             after(grammarAccess.getFeatureAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__NameAssignment_1"


    // $ANTLR start "rule__Feature__TypeAssignment_3"
    // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:784:1: rule__Feature__TypeAssignment_3 : ( ( RULE_ID ) ) ;
    public final void rule__Feature__TypeAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:788:1: ( ( ( RULE_ID ) ) )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:789:1: ( ( RULE_ID ) )
            {
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:789:1: ( ( RULE_ID ) )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:790:1: ( RULE_ID )
            {
             before(grammarAccess.getFeatureAccess().getTypeTypeCrossReference_3_0()); 
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:791:1: ( RULE_ID )
            // ../com.np.dsl.xtext.domainmodel.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalDomainModel.g:792:1: RULE_ID
            {
             before(grammarAccess.getFeatureAccess().getTypeTypeIDTerminalRuleCall_3_0_1()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Feature__TypeAssignment_31536); 
             after(grammarAccess.getFeatureAccess().getTypeTypeIDTerminalRuleCall_3_0_1()); 

            }

             after(grammarAccess.getFeatureAccess().getTypeTypeCrossReference_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__TypeAssignment_3"

    // Delegated rules


 

    public static final BitSet FOLLOW_ruleDomainmodel_in_entryRuleDomainmodel61 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleDomainmodel68 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Domainmodel__ElementsAssignment_in_ruleDomainmodel94 = new BitSet(new long[]{0x0000000000001802L});
    public static final BitSet FOLLOW_ruleType_in_entryRuleType122 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleType129 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Type__Alternatives_in_ruleType155 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleDataType_in_entryRuleDataType182 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleDataType189 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__DataType__Group__0_in_ruleDataType215 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleEntity_in_entryRuleEntity242 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleEntity249 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Entity__Group__0_in_ruleEntity275 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFeature_in_entryRuleFeature302 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleFeature309 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Feature__Group__0_in_ruleFeature335 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleDataType_in_rule__Type__Alternatives371 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleEntity_in_rule__Type__Alternatives388 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__DataType__Group__0__Impl_in_rule__DataType__Group__0418 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__DataType__Group__1_in_rule__DataType__Group__0421 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_11_in_rule__DataType__Group__0__Impl449 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__DataType__Group__1__Impl_in_rule__DataType__Group__1480 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__DataType__NameAssignment_1_in_rule__DataType__Group__1__Impl507 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Entity__Group__0__Impl_in_rule__Entity__Group__0541 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Entity__Group__1_in_rule__Entity__Group__0544 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_12_in_rule__Entity__Group__0__Impl572 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Entity__Group__1__Impl_in_rule__Entity__Group__1603 = new BitSet(new long[]{0x000000000000A000L});
    public static final BitSet FOLLOW_rule__Entity__Group__2_in_rule__Entity__Group__1606 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Entity__NameAssignment_1_in_rule__Entity__Group__1__Impl633 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Entity__Group__2__Impl_in_rule__Entity__Group__2663 = new BitSet(new long[]{0x000000000000A000L});
    public static final BitSet FOLLOW_rule__Entity__Group__3_in_rule__Entity__Group__2666 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Entity__Group_2__0_in_rule__Entity__Group__2__Impl693 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Entity__Group__3__Impl_in_rule__Entity__Group__3724 = new BitSet(new long[]{0x0000000000024010L});
    public static final BitSet FOLLOW_rule__Entity__Group__4_in_rule__Entity__Group__3727 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_13_in_rule__Entity__Group__3__Impl755 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Entity__Group__4__Impl_in_rule__Entity__Group__4786 = new BitSet(new long[]{0x0000000000024010L});
    public static final BitSet FOLLOW_rule__Entity__Group__5_in_rule__Entity__Group__4789 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Entity__FeaturesAssignment_4_in_rule__Entity__Group__4__Impl816 = new BitSet(new long[]{0x0000000000020012L});
    public static final BitSet FOLLOW_rule__Entity__Group__5__Impl_in_rule__Entity__Group__5847 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_14_in_rule__Entity__Group__5__Impl875 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Entity__Group_2__0__Impl_in_rule__Entity__Group_2__0918 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Entity__Group_2__1_in_rule__Entity__Group_2__0921 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_15_in_rule__Entity__Group_2__0__Impl949 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Entity__Group_2__1__Impl_in_rule__Entity__Group_2__1980 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Entity__SuperTypeAssignment_2_1_in_rule__Entity__Group_2__1__Impl1007 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Feature__Group__0__Impl_in_rule__Feature__Group__01041 = new BitSet(new long[]{0x0000000000020010L});
    public static final BitSet FOLLOW_rule__Feature__Group__1_in_rule__Feature__Group__01044 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Feature__ManyAssignment_0_in_rule__Feature__Group__0__Impl1071 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Feature__Group__1__Impl_in_rule__Feature__Group__11102 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_rule__Feature__Group__2_in_rule__Feature__Group__11105 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Feature__NameAssignment_1_in_rule__Feature__Group__1__Impl1132 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Feature__Group__2__Impl_in_rule__Feature__Group__21162 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Feature__Group__3_in_rule__Feature__Group__21165 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_16_in_rule__Feature__Group__2__Impl1193 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Feature__Group__3__Impl_in_rule__Feature__Group__31224 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Feature__TypeAssignment_3_in_rule__Feature__Group__3__Impl1251 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleType_in_rule__Domainmodel__ElementsAssignment1294 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__DataType__NameAssignment_11325 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Entity__NameAssignment_11356 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Entity__SuperTypeAssignment_2_11391 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFeature_in_rule__Entity__FeaturesAssignment_41426 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_17_in_rule__Feature__ManyAssignment_01462 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Feature__NameAssignment_11501 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Feature__TypeAssignment_31536 = new BitSet(new long[]{0x0000000000000002L});

}