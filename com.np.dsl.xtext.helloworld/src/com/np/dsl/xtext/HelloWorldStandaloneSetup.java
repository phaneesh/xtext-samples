
package com.np.dsl.xtext;

/**
 * Initialization support for running Xtext languages 
 * without equinox extension registry
 */
public class HelloWorldStandaloneSetup extends HelloWorldStandaloneSetupGenerated{

	public static void doSetup() {
		new HelloWorldStandaloneSetup().createInjectorAndDoEMFRegistration();
	}
}

