/**
 * <copyright>
 * </copyright>
 *

 */
package org.xtext.example.mydsl.domainModel.impl;

import org.eclipse.emf.ecore.EClass;

import org.xtext.example.mydsl.domainModel.DataType;
import org.xtext.example.mydsl.domainModel.DomainModelPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Data Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class DataTypeImpl extends TypeImpl implements DataType
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected DataTypeImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return DomainModelPackage.Literals.DATA_TYPE;
  }

} //DataTypeImpl
