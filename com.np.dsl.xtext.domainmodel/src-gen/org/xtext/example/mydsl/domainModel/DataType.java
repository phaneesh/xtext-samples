/**
 * <copyright>
 * </copyright>
 *

 */
package org.xtext.example.mydsl.domainModel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.xtext.example.mydsl.domainModel.DomainModelPackage#getDataType()
 * @model
 * @generated
 */
public interface DataType extends Type
{
} // DataType
