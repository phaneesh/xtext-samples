package org.xtext.example.mydsl.parser.antlr.internal; 

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import org.xtext.example.mydsl.services.DomainModelGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalDomainModelParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'datatype'", "'entity'", "'extends'", "'{'", "'}'", "'many'", "':'"
    };
    public static final int RULE_ID=4;
    public static final int RULE_STRING=6;
    public static final int T__16=16;
    public static final int T__15=15;
    public static final int T__17=17;
    public static final int T__12=12;
    public static final int T__11=11;
    public static final int T__14=14;
    public static final int T__13=13;
    public static final int RULE_ANY_OTHER=10;
    public static final int RULE_INT=5;
    public static final int RULE_WS=9;
    public static final int RULE_SL_COMMENT=8;
    public static final int EOF=-1;
    public static final int RULE_ML_COMMENT=7;

    // delegates
    // delegators


        public InternalDomainModelParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalDomainModelParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalDomainModelParser.tokenNames; }
    public String getGrammarFileName() { return "../com.np.dsl.xtext.domainmodel/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDomainModel.g"; }



     	private DomainModelGrammarAccess grammarAccess;
     	
        public InternalDomainModelParser(TokenStream input, DomainModelGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }
        
        @Override
        protected String getFirstRuleName() {
        	return "Domainmodel";	
       	}
       	
       	@Override
       	protected DomainModelGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}



    // $ANTLR start "entryRuleDomainmodel"
    // ../com.np.dsl.xtext.domainmodel/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDomainModel.g:67:1: entryRuleDomainmodel returns [EObject current=null] : iv_ruleDomainmodel= ruleDomainmodel EOF ;
    public final EObject entryRuleDomainmodel() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDomainmodel = null;


        try {
            // ../com.np.dsl.xtext.domainmodel/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDomainModel.g:68:2: (iv_ruleDomainmodel= ruleDomainmodel EOF )
            // ../com.np.dsl.xtext.domainmodel/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDomainModel.g:69:2: iv_ruleDomainmodel= ruleDomainmodel EOF
            {
             newCompositeNode(grammarAccess.getDomainmodelRule()); 
            pushFollow(FOLLOW_ruleDomainmodel_in_entryRuleDomainmodel75);
            iv_ruleDomainmodel=ruleDomainmodel();

            state._fsp--;

             current =iv_ruleDomainmodel; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleDomainmodel85); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDomainmodel"


    // $ANTLR start "ruleDomainmodel"
    // ../com.np.dsl.xtext.domainmodel/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDomainModel.g:76:1: ruleDomainmodel returns [EObject current=null] : ( (lv_elements_0_0= ruleType ) )* ;
    public final EObject ruleDomainmodel() throws RecognitionException {
        EObject current = null;

        EObject lv_elements_0_0 = null;


         enterRule(); 
            
        try {
            // ../com.np.dsl.xtext.domainmodel/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDomainModel.g:79:28: ( ( (lv_elements_0_0= ruleType ) )* )
            // ../com.np.dsl.xtext.domainmodel/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDomainModel.g:80:1: ( (lv_elements_0_0= ruleType ) )*
            {
            // ../com.np.dsl.xtext.domainmodel/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDomainModel.g:80:1: ( (lv_elements_0_0= ruleType ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( ((LA1_0>=11 && LA1_0<=12)) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // ../com.np.dsl.xtext.domainmodel/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDomainModel.g:81:1: (lv_elements_0_0= ruleType )
            	    {
            	    // ../com.np.dsl.xtext.domainmodel/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDomainModel.g:81:1: (lv_elements_0_0= ruleType )
            	    // ../com.np.dsl.xtext.domainmodel/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDomainModel.g:82:3: lv_elements_0_0= ruleType
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getDomainmodelAccess().getElementsTypeParserRuleCall_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleType_in_ruleDomainmodel130);
            	    lv_elements_0_0=ruleType();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getDomainmodelRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"elements",
            	            		lv_elements_0_0, 
            	            		"Type");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDomainmodel"


    // $ANTLR start "entryRuleType"
    // ../com.np.dsl.xtext.domainmodel/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDomainModel.g:106:1: entryRuleType returns [EObject current=null] : iv_ruleType= ruleType EOF ;
    public final EObject entryRuleType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleType = null;


        try {
            // ../com.np.dsl.xtext.domainmodel/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDomainModel.g:107:2: (iv_ruleType= ruleType EOF )
            // ../com.np.dsl.xtext.domainmodel/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDomainModel.g:108:2: iv_ruleType= ruleType EOF
            {
             newCompositeNode(grammarAccess.getTypeRule()); 
            pushFollow(FOLLOW_ruleType_in_entryRuleType166);
            iv_ruleType=ruleType();

            state._fsp--;

             current =iv_ruleType; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleType176); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleType"


    // $ANTLR start "ruleType"
    // ../com.np.dsl.xtext.domainmodel/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDomainModel.g:115:1: ruleType returns [EObject current=null] : (this_DataType_0= ruleDataType | this_Entity_1= ruleEntity ) ;
    public final EObject ruleType() throws RecognitionException {
        EObject current = null;

        EObject this_DataType_0 = null;

        EObject this_Entity_1 = null;


         enterRule(); 
            
        try {
            // ../com.np.dsl.xtext.domainmodel/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDomainModel.g:118:28: ( (this_DataType_0= ruleDataType | this_Entity_1= ruleEntity ) )
            // ../com.np.dsl.xtext.domainmodel/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDomainModel.g:119:1: (this_DataType_0= ruleDataType | this_Entity_1= ruleEntity )
            {
            // ../com.np.dsl.xtext.domainmodel/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDomainModel.g:119:1: (this_DataType_0= ruleDataType | this_Entity_1= ruleEntity )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==11) ) {
                alt2=1;
            }
            else if ( (LA2_0==12) ) {
                alt2=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // ../com.np.dsl.xtext.domainmodel/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDomainModel.g:120:5: this_DataType_0= ruleDataType
                    {
                     
                            newCompositeNode(grammarAccess.getTypeAccess().getDataTypeParserRuleCall_0()); 
                        
                    pushFollow(FOLLOW_ruleDataType_in_ruleType223);
                    this_DataType_0=ruleDataType();

                    state._fsp--;

                     
                            current = this_DataType_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../com.np.dsl.xtext.domainmodel/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDomainModel.g:130:5: this_Entity_1= ruleEntity
                    {
                     
                            newCompositeNode(grammarAccess.getTypeAccess().getEntityParserRuleCall_1()); 
                        
                    pushFollow(FOLLOW_ruleEntity_in_ruleType250);
                    this_Entity_1=ruleEntity();

                    state._fsp--;

                     
                            current = this_Entity_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleType"


    // $ANTLR start "entryRuleDataType"
    // ../com.np.dsl.xtext.domainmodel/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDomainModel.g:146:1: entryRuleDataType returns [EObject current=null] : iv_ruleDataType= ruleDataType EOF ;
    public final EObject entryRuleDataType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDataType = null;


        try {
            // ../com.np.dsl.xtext.domainmodel/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDomainModel.g:147:2: (iv_ruleDataType= ruleDataType EOF )
            // ../com.np.dsl.xtext.domainmodel/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDomainModel.g:148:2: iv_ruleDataType= ruleDataType EOF
            {
             newCompositeNode(grammarAccess.getDataTypeRule()); 
            pushFollow(FOLLOW_ruleDataType_in_entryRuleDataType285);
            iv_ruleDataType=ruleDataType();

            state._fsp--;

             current =iv_ruleDataType; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleDataType295); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDataType"


    // $ANTLR start "ruleDataType"
    // ../com.np.dsl.xtext.domainmodel/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDomainModel.g:155:1: ruleDataType returns [EObject current=null] : (otherlv_0= 'datatype' ( (lv_name_1_0= RULE_ID ) ) ) ;
    public final EObject ruleDataType() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;

         enterRule(); 
            
        try {
            // ../com.np.dsl.xtext.domainmodel/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDomainModel.g:158:28: ( (otherlv_0= 'datatype' ( (lv_name_1_0= RULE_ID ) ) ) )
            // ../com.np.dsl.xtext.domainmodel/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDomainModel.g:159:1: (otherlv_0= 'datatype' ( (lv_name_1_0= RULE_ID ) ) )
            {
            // ../com.np.dsl.xtext.domainmodel/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDomainModel.g:159:1: (otherlv_0= 'datatype' ( (lv_name_1_0= RULE_ID ) ) )
            // ../com.np.dsl.xtext.domainmodel/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDomainModel.g:159:3: otherlv_0= 'datatype' ( (lv_name_1_0= RULE_ID ) )
            {
            otherlv_0=(Token)match(input,11,FOLLOW_11_in_ruleDataType332); 

                	newLeafNode(otherlv_0, grammarAccess.getDataTypeAccess().getDatatypeKeyword_0());
                
            // ../com.np.dsl.xtext.domainmodel/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDomainModel.g:163:1: ( (lv_name_1_0= RULE_ID ) )
            // ../com.np.dsl.xtext.domainmodel/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDomainModel.g:164:1: (lv_name_1_0= RULE_ID )
            {
            // ../com.np.dsl.xtext.domainmodel/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDomainModel.g:164:1: (lv_name_1_0= RULE_ID )
            // ../com.np.dsl.xtext.domainmodel/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDomainModel.g:165:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleDataType349); 

            			newLeafNode(lv_name_1_0, grammarAccess.getDataTypeAccess().getNameIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getDataTypeRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"ID");
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDataType"


    // $ANTLR start "entryRuleEntity"
    // ../com.np.dsl.xtext.domainmodel/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDomainModel.g:189:1: entryRuleEntity returns [EObject current=null] : iv_ruleEntity= ruleEntity EOF ;
    public final EObject entryRuleEntity() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEntity = null;


        try {
            // ../com.np.dsl.xtext.domainmodel/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDomainModel.g:190:2: (iv_ruleEntity= ruleEntity EOF )
            // ../com.np.dsl.xtext.domainmodel/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDomainModel.g:191:2: iv_ruleEntity= ruleEntity EOF
            {
             newCompositeNode(grammarAccess.getEntityRule()); 
            pushFollow(FOLLOW_ruleEntity_in_entryRuleEntity390);
            iv_ruleEntity=ruleEntity();

            state._fsp--;

             current =iv_ruleEntity; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleEntity400); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEntity"


    // $ANTLR start "ruleEntity"
    // ../com.np.dsl.xtext.domainmodel/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDomainModel.g:198:1: ruleEntity returns [EObject current=null] : (otherlv_0= 'entity' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= 'extends' ( (otherlv_3= RULE_ID ) ) )? otherlv_4= '{' ( (lv_features_5_0= ruleFeature ) )* otherlv_6= '}' ) ;
    public final EObject ruleEntity() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        EObject lv_features_5_0 = null;


         enterRule(); 
            
        try {
            // ../com.np.dsl.xtext.domainmodel/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDomainModel.g:201:28: ( (otherlv_0= 'entity' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= 'extends' ( (otherlv_3= RULE_ID ) ) )? otherlv_4= '{' ( (lv_features_5_0= ruleFeature ) )* otherlv_6= '}' ) )
            // ../com.np.dsl.xtext.domainmodel/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDomainModel.g:202:1: (otherlv_0= 'entity' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= 'extends' ( (otherlv_3= RULE_ID ) ) )? otherlv_4= '{' ( (lv_features_5_0= ruleFeature ) )* otherlv_6= '}' )
            {
            // ../com.np.dsl.xtext.domainmodel/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDomainModel.g:202:1: (otherlv_0= 'entity' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= 'extends' ( (otherlv_3= RULE_ID ) ) )? otherlv_4= '{' ( (lv_features_5_0= ruleFeature ) )* otherlv_6= '}' )
            // ../com.np.dsl.xtext.domainmodel/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDomainModel.g:202:3: otherlv_0= 'entity' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= 'extends' ( (otherlv_3= RULE_ID ) ) )? otherlv_4= '{' ( (lv_features_5_0= ruleFeature ) )* otherlv_6= '}'
            {
            otherlv_0=(Token)match(input,12,FOLLOW_12_in_ruleEntity437); 

                	newLeafNode(otherlv_0, grammarAccess.getEntityAccess().getEntityKeyword_0());
                
            // ../com.np.dsl.xtext.domainmodel/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDomainModel.g:206:1: ( (lv_name_1_0= RULE_ID ) )
            // ../com.np.dsl.xtext.domainmodel/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDomainModel.g:207:1: (lv_name_1_0= RULE_ID )
            {
            // ../com.np.dsl.xtext.domainmodel/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDomainModel.g:207:1: (lv_name_1_0= RULE_ID )
            // ../com.np.dsl.xtext.domainmodel/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDomainModel.g:208:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleEntity454); 

            			newLeafNode(lv_name_1_0, grammarAccess.getEntityAccess().getNameIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getEntityRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"ID");
            	    

            }


            }

            // ../com.np.dsl.xtext.domainmodel/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDomainModel.g:224:2: (otherlv_2= 'extends' ( (otherlv_3= RULE_ID ) ) )?
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==13) ) {
                alt3=1;
            }
            switch (alt3) {
                case 1 :
                    // ../com.np.dsl.xtext.domainmodel/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDomainModel.g:224:4: otherlv_2= 'extends' ( (otherlv_3= RULE_ID ) )
                    {
                    otherlv_2=(Token)match(input,13,FOLLOW_13_in_ruleEntity472); 

                        	newLeafNode(otherlv_2, grammarAccess.getEntityAccess().getExtendsKeyword_2_0());
                        
                    // ../com.np.dsl.xtext.domainmodel/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDomainModel.g:228:1: ( (otherlv_3= RULE_ID ) )
                    // ../com.np.dsl.xtext.domainmodel/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDomainModel.g:229:1: (otherlv_3= RULE_ID )
                    {
                    // ../com.np.dsl.xtext.domainmodel/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDomainModel.g:229:1: (otherlv_3= RULE_ID )
                    // ../com.np.dsl.xtext.domainmodel/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDomainModel.g:230:3: otherlv_3= RULE_ID
                    {

                    			if (current==null) {
                    	            current = createModelElement(grammarAccess.getEntityRule());
                    	        }
                            
                    otherlv_3=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleEntity492); 

                    		newLeafNode(otherlv_3, grammarAccess.getEntityAccess().getSuperTypeEntityCrossReference_2_1_0()); 
                    	

                    }


                    }


                    }
                    break;

            }

            otherlv_4=(Token)match(input,14,FOLLOW_14_in_ruleEntity506); 

                	newLeafNode(otherlv_4, grammarAccess.getEntityAccess().getLeftCurlyBracketKeyword_3());
                
            // ../com.np.dsl.xtext.domainmodel/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDomainModel.g:245:1: ( (lv_features_5_0= ruleFeature ) )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==RULE_ID||LA4_0==16) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // ../com.np.dsl.xtext.domainmodel/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDomainModel.g:246:1: (lv_features_5_0= ruleFeature )
            	    {
            	    // ../com.np.dsl.xtext.domainmodel/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDomainModel.g:246:1: (lv_features_5_0= ruleFeature )
            	    // ../com.np.dsl.xtext.domainmodel/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDomainModel.g:247:3: lv_features_5_0= ruleFeature
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getEntityAccess().getFeaturesFeatureParserRuleCall_4_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleFeature_in_ruleEntity527);
            	    lv_features_5_0=ruleFeature();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getEntityRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"features",
            	            		lv_features_5_0, 
            	            		"Feature");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);

            otherlv_6=(Token)match(input,15,FOLLOW_15_in_ruleEntity540); 

                	newLeafNode(otherlv_6, grammarAccess.getEntityAccess().getRightCurlyBracketKeyword_5());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEntity"


    // $ANTLR start "entryRuleFeature"
    // ../com.np.dsl.xtext.domainmodel/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDomainModel.g:275:1: entryRuleFeature returns [EObject current=null] : iv_ruleFeature= ruleFeature EOF ;
    public final EObject entryRuleFeature() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFeature = null;


        try {
            // ../com.np.dsl.xtext.domainmodel/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDomainModel.g:276:2: (iv_ruleFeature= ruleFeature EOF )
            // ../com.np.dsl.xtext.domainmodel/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDomainModel.g:277:2: iv_ruleFeature= ruleFeature EOF
            {
             newCompositeNode(grammarAccess.getFeatureRule()); 
            pushFollow(FOLLOW_ruleFeature_in_entryRuleFeature576);
            iv_ruleFeature=ruleFeature();

            state._fsp--;

             current =iv_ruleFeature; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleFeature586); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFeature"


    // $ANTLR start "ruleFeature"
    // ../com.np.dsl.xtext.domainmodel/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDomainModel.g:284:1: ruleFeature returns [EObject current=null] : ( ( (lv_many_0_0= 'many' ) )? ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (otherlv_3= RULE_ID ) ) ) ;
    public final EObject ruleFeature() throws RecognitionException {
        EObject current = null;

        Token lv_many_0_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;

         enterRule(); 
            
        try {
            // ../com.np.dsl.xtext.domainmodel/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDomainModel.g:287:28: ( ( ( (lv_many_0_0= 'many' ) )? ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (otherlv_3= RULE_ID ) ) ) )
            // ../com.np.dsl.xtext.domainmodel/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDomainModel.g:288:1: ( ( (lv_many_0_0= 'many' ) )? ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (otherlv_3= RULE_ID ) ) )
            {
            // ../com.np.dsl.xtext.domainmodel/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDomainModel.g:288:1: ( ( (lv_many_0_0= 'many' ) )? ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (otherlv_3= RULE_ID ) ) )
            // ../com.np.dsl.xtext.domainmodel/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDomainModel.g:288:2: ( (lv_many_0_0= 'many' ) )? ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (otherlv_3= RULE_ID ) )
            {
            // ../com.np.dsl.xtext.domainmodel/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDomainModel.g:288:2: ( (lv_many_0_0= 'many' ) )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==16) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // ../com.np.dsl.xtext.domainmodel/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDomainModel.g:289:1: (lv_many_0_0= 'many' )
                    {
                    // ../com.np.dsl.xtext.domainmodel/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDomainModel.g:289:1: (lv_many_0_0= 'many' )
                    // ../com.np.dsl.xtext.domainmodel/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDomainModel.g:290:3: lv_many_0_0= 'many'
                    {
                    lv_many_0_0=(Token)match(input,16,FOLLOW_16_in_ruleFeature629); 

                            newLeafNode(lv_many_0_0, grammarAccess.getFeatureAccess().getManyManyKeyword_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getFeatureRule());
                    	        }
                           		setWithLastConsumed(current, "many", true, "many");
                    	    

                    }


                    }
                    break;

            }

            // ../com.np.dsl.xtext.domainmodel/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDomainModel.g:303:3: ( (lv_name_1_0= RULE_ID ) )
            // ../com.np.dsl.xtext.domainmodel/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDomainModel.g:304:1: (lv_name_1_0= RULE_ID )
            {
            // ../com.np.dsl.xtext.domainmodel/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDomainModel.g:304:1: (lv_name_1_0= RULE_ID )
            // ../com.np.dsl.xtext.domainmodel/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDomainModel.g:305:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleFeature660); 

            			newLeafNode(lv_name_1_0, grammarAccess.getFeatureAccess().getNameIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getFeatureRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"ID");
            	    

            }


            }

            otherlv_2=(Token)match(input,17,FOLLOW_17_in_ruleFeature677); 

                	newLeafNode(otherlv_2, grammarAccess.getFeatureAccess().getColonKeyword_2());
                
            // ../com.np.dsl.xtext.domainmodel/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDomainModel.g:325:1: ( (otherlv_3= RULE_ID ) )
            // ../com.np.dsl.xtext.domainmodel/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDomainModel.g:326:1: (otherlv_3= RULE_ID )
            {
            // ../com.np.dsl.xtext.domainmodel/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDomainModel.g:326:1: (otherlv_3= RULE_ID )
            // ../com.np.dsl.xtext.domainmodel/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalDomainModel.g:327:3: otherlv_3= RULE_ID
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getFeatureRule());
            	        }
                    
            otherlv_3=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleFeature697); 

            		newLeafNode(otherlv_3, grammarAccess.getFeatureAccess().getTypeTypeCrossReference_3_0()); 
            	

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFeature"

    // Delegated rules


 

    public static final BitSet FOLLOW_ruleDomainmodel_in_entryRuleDomainmodel75 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleDomainmodel85 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleType_in_ruleDomainmodel130 = new BitSet(new long[]{0x0000000000001802L});
    public static final BitSet FOLLOW_ruleType_in_entryRuleType166 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleType176 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleDataType_in_ruleType223 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleEntity_in_ruleType250 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleDataType_in_entryRuleDataType285 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleDataType295 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_11_in_ruleDataType332 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleDataType349 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleEntity_in_entryRuleEntity390 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleEntity400 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_12_in_ruleEntity437 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleEntity454 = new BitSet(new long[]{0x0000000000006000L});
    public static final BitSet FOLLOW_13_in_ruleEntity472 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleEntity492 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_14_in_ruleEntity506 = new BitSet(new long[]{0x0000000000018010L});
    public static final BitSet FOLLOW_ruleFeature_in_ruleEntity527 = new BitSet(new long[]{0x0000000000018010L});
    public static final BitSet FOLLOW_15_in_ruleEntity540 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFeature_in_entryRuleFeature576 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleFeature586 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_16_in_ruleFeature629 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleFeature660 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_17_in_ruleFeature677 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleFeature697 = new BitSet(new long[]{0x0000000000000002L});

}